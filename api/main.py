from fastapi import FastAPI, Request, Response
from fastapi.openapi.utils import get_openapi
from starlette.middleware.cors import CORSMiddleware
from api.database import SessionLocal, engine, Base
from api.settings import settings
from api.routers import Code, Company, Jurisdiction

app = FastAPI()

Base.metadata.create_all(bind=engine)

@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    response = Response("Internal server error", status_code=500)
    try:
        request.state.db = SessionLocal()
        response = await call_next(request)
    finally:
        request.state.db.close()
    return response

def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Water-api",
        version="1.0.1",
        description="This is a the API for The Code For Europe Transparent Water Initiative",
        routes=app.routes,
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


# Dependency
def get_db(request: Request):
    return request.state.db

app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.BACKEND_CORS_ORIGINS_CSV.split(','),
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(Code.router)
app.include_router(Company.router)
app.include_router(Jurisdiction.router)
app.openapi = custom_openapi