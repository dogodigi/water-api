import os
from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = "Transparentwater API"
    DATABASE_URL: str = os.environ.get('DATABASE_URL', "postgresql://postgres:postgres@localhost/transparentwater")
    BACKEND_CORS_ORIGINS_CSV: str = os.environ.get('BACKEND_CORS_ORIGINS_CSV', "http://localhost,http://localhost:4200,http://localhost:3000")
    secret: str = "developmentrocks"
    locales: list = ["de", "en", "nl", "fr"]
    default_locale: str = "en"

settings = Settings()
