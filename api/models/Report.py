import enum
from sqlalchemy import Integer, String, Enum, Column, Date, DateTime, func, UnicodeText, ForeignKey, ARRAY
from sqlalchemy.orm import relationship
from api.database import Base
from api.settings import settings
from pydantic import BaseModel
from typing import Optional
from sqlalchemy_i18n import (
    translation_base
)


class Report(Base):
    __tablename__ = "reports"
    __translatable__ = {'locales': settings.locales}

    locale = settings.default_locale

    id = Column(Integer, index=True, primary_key=True)
    name = Column(String, unique=True, index=True)
    # Description from translation
    year = Column(Date(), server_default=func.now())
    issued = Column(Date(), server_default=func.now()) # Date on which the report is issued. If month or quarter is given, set to last day of month/quarter

    operator_id = Column(
        Integer,
        ForeignKey('companies.id', ondelete='CASCADE'),
    )
    operator = relationship('Company', backref='reports')

    authority_id = Column(
        Integer,
        ForeignKey('companies.id', ondelete='CASCADE'),
    )
    authority = relationship('Company', backref='reports')

    # Zones

    # Plants

    # Observations

    sources = Column(ARRAY(String))

    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
        nullable=False,
    )

    created_user = relationship('User', backref='reports')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='reports')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class ReportTranslation(translation_base(Report)):
    __tablename__ = 'report_translation'
    description = Column(UnicodeText)
    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
        nullable=False,
    )

    created_user = relationship('User', backref='report_translation')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='report_translation')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class ReportResponse(BaseModel):
    id: int
    name: str
    description: Optional[str]

    class Config:
        orm_mode = True


class ReportCreate(BaseModel):
    name: str
    description: Optional[str]
