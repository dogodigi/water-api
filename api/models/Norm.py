from sqlalchemy import Integer, String, Column, DateTime, ForeignKey, func, UnicodeText, ARRAY
from sqlalchemy.orm import relationship
from api.database import Base
from api.settings import settings
from pydantic import BaseModel
from typing import Optional, List
from sqlalchemy_i18n import (
    translation_base
)


class Norm(Base):
    __tablename__ = "norms"

    __translatable__ = {'locales': settings.locales}

    locale = settings.default_locale

    id = Column(Integer, index=True, primary_key=True)

    authority_id = Column(
        Integer,
        ForeignKey('companies.id', ondelete='CASCADE'),
    )
    authority = relationship('Company', backref='norms')

    name = Column(String, unique=True, index=True)
    sources = Column(ARRAY(String))
    # TODO: Add relation to Observations
    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )
    created_user = relationship('User', backref='zones')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='zones')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class NormTranslation(translation_base(Norm)):
    __tablename__ = 'norm_translation'
    description = Column(UnicodeText)

    created_user = relationship('User', backref='norm_translation')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='norm_translation')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class NormResponse(BaseModel):
    id: int
    name: str
    description: Optional[str]
    sources: Optional[List[str]] = []

    class Config:
        orm_mode = True


class NormCreate(BaseModel):
    name: str
    description: Optional[str]
    sources: Optional[List[str]] = []
