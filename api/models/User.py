import enum
from sqlalchemy import Integer, String, Enum, Column, DateTime, func, UnicodeText
from api.database import Base
from api.settings import settings
from pydantic import BaseModel
from typing import Optional


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, index=True, primary_key=True)
    name = Column(String, unique=True)


class UserResponse(BaseModel):
    id: int
    name: str

    class Config:
        orm_mode = True


class UserCreate(BaseModel):
    name: str
