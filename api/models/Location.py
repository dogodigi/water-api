import enum
from sqlalchemy import Integer, String, Enum, Column, DateTime, ForeignKey, func, UnicodeText
from sqlalchemy.orm import relationship
from geoalchemy2 import Geometry
from api.database import Base
from api.settings import settings
from pydantic import BaseModel
from typing import Optional
from sqlalchemy_i18n import (
    translation_base
)


class TypeEnum(str, enum.Enum):
    tap = "Tap"
    pump = "Pump"
    restaurant = "Restaurant"
    facility = "Facility"
    measurement = "Measurement"
    plant = "Plant"
    other = "Other"
    unknown = "Unknown"


class AccessEnum(str, enum.Enum):
    free = "Free"
    commercial = "Commercial"
    restricted = "Restricted"
    other = "Other"
    unknown = "Unknown"


class Location(Base):
    __tablename__ = "locations"

    locale = settings.default_locale

    id = Column(Integer, index=True, primary_key=True)

    operator_id = Column(
        Integer,
        ForeignKey('companies.id', ondelete='CASCADE'),
    )
    operator = relationship('Company', backref='locations')

    name = Column(String, unique=True, index=True)
    # description from translation
    geometry = Column(Geometry('POINT', 4326))
    type = Column(Enum(TypeEnum))
    access = Column(Enum(AccessEnum))

    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )
    created_user = relationship('User', backref='locations')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='locations')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class LocationTranslation(translation_base(Location)):
    __tablename__ = 'location_translation'
    description = Column(UnicodeText)

    created_user = relationship('User', backref='location_translation')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='location_translation')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class LocationResponse(BaseModel):
    id: int
    value: str
    label: Optional[str]
    description: Optional[str]

    class Config:
        orm_mode = True


class LocationCreate(BaseModel):
    value: str
    label: Optional[str]
    description: Optional[str]
