import enum
from sqlalchemy import Integer, String, Enum, Column, DateTime, func, UnicodeText, ForeignKey
from sqlalchemy.orm import relationship
from api.database import Base
from api.settings import settings
from pydantic import BaseModel
from typing import Optional
from sqlalchemy_i18n import (
    translation_base
)


class Uom(Base):
    __tablename__ = "uoms"
    __translatable__ = {'locales': settings.locales}

    locale = settings.default_locale

    id = Column(Integer, index=True, primary_key=True)
    code = Column(String, unique=True, index=True)

    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
        nullable=False,
    )

    created_user = relationship('User', backref='uoms')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='uoms')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class UomTranslation(translation_base(Uom)):
    __tablename__ = 'uom_translation'
    value = Column(UnicodeText)
    label = Column(UnicodeText)
    definition = Column(UnicodeText)

    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
        nullable=False,
    )

    created_user = relationship('User', backref='uom_translation')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='uom_translation')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class UomResponse(BaseModel):
    id: int
    code: str
    value: str
    label: Optional[str]
    definition: Optional[str]

    class Config:
        orm_mode = True


class UomCreate(BaseModel):
    code: str
    value: str
    label: Optional[str]
    definition: Optional[str]