from sqlalchemy import Integer, String, Column, DateTime, ForeignKey, func, Float, ARRAY
from sqlalchemy.orm import relationship
from api.database import Base
from pydantic import BaseModel
from typing import Optional, List


class Product(Base):
    __tablename__ = "products"

    id = Column(Integer, index=True, primary_key=True)
    code = Column(String, unique=True, index=True)
    name = Column(String, unique=True, index=True)
    vendor_id = Column(
        Integer,
        ForeignKey('companies.id', ondelete='CASCADE'),
    )
    vendor = relationship('Company', backref='product')
    # TODO: Add relation to Observations
    volume = Column(Float)
    sources = Column(ARRAY(String))

    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )
    created_user = relationship('User', backref='zones')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='zones')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class ProductResponse(BaseModel):
    id: int
    code: str
    name: str
    sources: Optional[List[str]] = []

    class Config:
        orm_mode = True


class ProductCreate(BaseModel):
    code: str
    name: str
    sources: Optional[List[str]] = []
