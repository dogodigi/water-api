import enum
from sqlalchemy import Integer, String, Enum, Column, DateTime, func, UnicodeText, ForeignKey
from sqlalchemy.orm import relationship
from api.database import Base
from api.settings import settings
from pydantic import BaseModel
from typing import Optional
from sqlalchemy_i18n import (
    translation_base
)


class StandardEnum(str, enum.Enum):
    case = "CAS"
    ec = "EC"
    icsc = "ICSC"
    rtecs = "RTECS"
    un = "UN"
    unii = "UNII"
    eea = "EEA"
    na = "NA"
    ms = "microbiology_society"
    other = "Other"
    unknown = "Unknown"


class Code(Base):
    __tablename__ = "codes"
    __translatable__ = {'locales': settings.locales}

    locale = settings.default_locale

    id = Column(Integer, index=True, primary_key=True)
    code = Column(String, unique=True, index=True)
    standard = Column(Enum(StandardEnum))

    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
        nullable=False,
    )

    created_user = relationship('User', backref='codes')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='codes')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class CodeTranslation(translation_base(Code)):
    __tablename__ = 'code_translation'
    description = Column(UnicodeText)
    label = Column(UnicodeText)
    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
        nullable=False,
    )

    created_user = relationship('User', backref='code_translation')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='code_translation')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class CodeResponse(BaseModel):
    id: int
    code: str
    label: Optional[str]
    standard: Optional[str]
    description: Optional[str]

    class Config:
        orm_mode = True


class CodeCreate(BaseModel):
    code: str
    label: Optional[str]
    standard: Optional[str]
    description: Optional[str]
