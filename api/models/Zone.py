import enum
from sqlalchemy import Integer, String, Enum, Column, DateTime, ForeignKey, func, UnicodeText, ARRAY
from sqlalchemy.orm import relationship
from geoalchemy2 import Geometry
from api.database import Base
from api.settings import settings
from pydantic import BaseModel
from typing import Optional, List
from sqlalchemy_i18n import (
    make_translatable,
    translation_base,
    Translatable,
)

class Zone(Base):
    __tablename__ = "zones"

    id = Column(Integer, index=True, primary_key=True)

    operator_id = Column(
        Integer,
        ForeignKey('companies.id', ondelete='CASCADE'),
    )
    operator = relationship('Company', backref='zones')

    name = Column(String, unique=True, index=True)
    alternatives = Column(ARRAY(String))
    geometry = Column(Geometry('POLYGON',4326))

    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )
    created_user = relationship('User', backref='zones')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='zones')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class ZoneResponse(BaseModel):
    id: int
    name: str
    alternatives: Optional[List[str]] = []

    class Config:
        orm_mode = True


class ZoneCreate(BaseModel):
    name: str
    alternatives: Optional[List[str]] = []
