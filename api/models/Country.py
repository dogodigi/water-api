from sqlalchemy import Integer, String, Column, DateTime, ForeignKey, func, UnicodeText
from sqlalchemy.orm import relationship
from api.database import Base
from api.settings import settings
from pydantic import BaseModel
from typing import Optional
from sqlalchemy_i18n import (
    translation_base
)


class Country(Base):
    __tablename__ = "countries"

    locale = settings.default_locale

    id = Column(Integer, index=True, primary_key=True)
    value = Column(String, unique=True, index=True)
    label = Column(String)

    created_user = relationship('User', backref='countries')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='countries')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class CountryTranslation(translation_base(Country)):
    __tablename__ = 'country_translation'
    description = Column(UnicodeText)

    created_user = relationship('User', backref='country_translation')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='country_translation')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class CountryResponse(BaseModel):
    id: int
    value: str
    label: Optional[str]
    description: Optional[str]

    class Config:
        orm_mode = True


class CountryCreate(BaseModel):
    value: str
    label: Optional[str]
    description: Optional[str]
