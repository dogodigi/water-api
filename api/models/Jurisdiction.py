from pydantic import BaseModel
from typing import Optional

class Osm(BaseModel):
    type: str
    id: str

class JurisdictionResponse(BaseModel):
    id: int
    jurisdiction: str
    type: Optional[str] = "local government"
    country: str
    osm: Osm
