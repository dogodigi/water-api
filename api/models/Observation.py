import enum
from sqlalchemy import Integer, BigInteger, Float, Enum, Column, DateTime, ForeignKey, func
from sqlalchemy.orm import relationship
from api.database import Base
from pydantic import BaseModel
from typing import Optional


class TypeEnum(str, enum.Enum):
    report = "Report"
    norm = "Norm"
    location = "Location"
    zone = "Zone"
    label = "Label"
    other = "Other"
    unknown = "Unknown"


class Observation(Base):
    __tablename__ = "observations"

    id = Column(BigInteger, index=True, primary_key=True)
    value = Column(Float)
    min = Column(Float)
    max = Column(Float)
    samples = Column(Integer)
    uom_id = Column(
        Integer,
        ForeignKey('uoms.id', ondelete='CASCADE'),
    )
    uom = relationship('Uom', backref='observations')

    code_id = Column(
        Integer,
        ForeignKey('codes.id', ondelete='CASCADE'),
    )
    code = relationship('Code', backref='observations')
    type = Column(Enum(TypeEnum))

    created_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )
    created_user = relationship('User', backref='zones')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='zones')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class ObservationResponse(BaseModel):
    id: int
    value: float
    min: Optional[float]
    max: Optional[float]
    samples: int

    class Config:
        orm_mode = True


class ObservationCreate(BaseModel):
    value: float
    min: Optional[float]
    max: Optional[float]
    samples: int
