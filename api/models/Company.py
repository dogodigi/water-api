import enum
from sqlalchemy import Integer, String, Enum, Column, DateTime, ForeignKey, func
from sqlalchemy.orm import relationship
from api.database import Base
from pydantic import BaseModel
from typing import Optional


class TypeEnum(str, enum.Enum):
    access = "Access"
    provider = "Provide"
    infrastructure = "Infrastructure"
    vendor = "Vendor"
    unknown = "Unknown"
    government = "Government"
    quality = "Quality"
    other = "Other"


class Company(Base):
    __tablename__ = "companies"

    id = Column(Integer, index=True, primary_key=True)
    code = Column(String, unique=True, index=True)
    name = Column(String)
    url = Column(String)
    country = Column(String)
    type = Column(Enum(TypeEnum))

    created_user = relationship('User', backref='companies')
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    updated_by = Column(
        Integer,
        ForeignKey('users.id', ondelete='CASCADE'),
    )

    updated_user = relationship('User', backref='companies')
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())


class CompanyResponse(BaseModel):
    id: int
    code: str
    name: Optional[str]
    url: Optional[str]
    country: Optional[str]
    type: TypeEnum

    class Config:
        orm_mode = True


class CompanyCreate(BaseModel):
    code: str
    name: Optional[str]
    url: Optional[str]
    country: Optional[str]
    type: TypeEnum
