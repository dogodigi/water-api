from typing import List
from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from api.models import Company, CompanyResponse, CompanyCreate

router = APIRouter()

def get_base_query(db):
    return db.query(
        Company.id,
        Company.code,
        Company.name,
        Company.url,
        Company.country,
        Company.type
    )


# Dependency
def get_db(request: Request):
    return request.state.db


@router.get("/api/v1/companies", response_model=List[CompanyResponse], response_model_exclude_none=True)
def get_companies(db: Session = Depends(get_db)):
    return get_base_query(db).all()


@router.get("/api/v1/company/{id}", response_model=CompanyResponse, response_model_exclude_none=True)
def get_company_by_id(id, db: Session = Depends(get_db)):
    return get_base_query(db).filter(Company.id == id).first()


@router.get("/api/v1/company", response_model=List[CompanyResponse], response_model_exclude_none=True)
def search_company(search: str = "", limit: int = 10, db: Session = Depends(get_db)):
    search_filter = "%{}%".format(search)
    return get_base_query(db).filter(Company.name.ilike(search_filter)).limit(limit).all()


@router.post("/api/v1/company", response_model=CompanyResponse, response_model_exclude_none=True)
def post_company(company: CompanyCreate, db: Session = Depends(get_db)):
    CompanyAdd = Company(
        code=company.code,
        name=company.name,
        url=company.url,
        country=company.country,
        type=company.type
    )
    db.add(CompanyAdd)
    db.commit()
    db.refresh(CompanyAdd)
    return CompanyAdd
