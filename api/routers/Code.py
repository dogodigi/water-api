from typing import List
from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session
from api.models import Code, CodeResponse

router = APIRouter()

def get_base_query(db):
    return db.query(
        Code.id,
        Code.standard
    )


# Dependency
def get_db(request: Request):
    return request.state.db


@router.get("/api/v1/codes", response_model=List[CodeResponse], response_model_exclude_none=True)
def get_all_codes(db: Session = Depends(get_db)):
    return get_base_query(db).all()