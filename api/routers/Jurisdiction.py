from fastapi import APIRouter, Request
from api.models import JurisdictionResponse
from geopy.geocoders import Nominatim
router = APIRouter()

# Depends on nominatim to return information about a local government

@router.get("/api/v1/jurisdiction", response_model_exclude_none=True)
def get_jurisdiction_for_a_given_area(q: str, lat: float = None, lon: float = None):
    geolocator = Nominatim(user_agent="codeforeurope-water-api")
    location = geolocator.geocode("2, Beukenlaan, Vught")
    print(location)
    return location.raw
