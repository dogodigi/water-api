var app = require('./api/main');
var http = require('http');
var serverPort = process.env.PORT || 8001;
http.createServer(app).listen(serverPort, function() {
  console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
  console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
});
