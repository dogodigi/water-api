#!/bin/bash
sed -i "s^__MONGO_SERVER__^$MONGO_SERVER^g" /water-api/api/settings.json
sed -i "s^__SECRET__^$SECRET^g" /water-api/api/settings.json
sed -i "s^__DB_PREFIX__^$DB_PREFIX^g" /water-api/api/settings.json

cd /water-api
cd assets
git pull
cd ..
npm start
